﻿namespace Conf
{
    public class UnitInfo
    {
        public int MaxHealth;
        public int MaxMana;
        public int MaxShieldStrength; //Мой код.
        public int ManaRegen;
        public int Speed;
        public int Damage;
        public int AttackDistance;
    }
}