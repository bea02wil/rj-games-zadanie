﻿namespace Conf
{
    public class MeleeMonkUnitInfo : UnitInfo //Мой класс.
    {
        public int ShieldStrength;
        public int HealValue;

        public MeleeMonkUnitInfo()
        {
            MaxHealth = 1800;
            MaxMana = 100;
            MaxShieldStrength = 100;
            Speed = 3;
            Damage = 5;
            ManaRegen = 5;
            AttackDistance = 2;
            ShieldStrength = 100;
            HealValue = 100;
        }
    }
}


