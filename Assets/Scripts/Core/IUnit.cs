﻿namespace Core
{
    public interface IUnit
    {
        TeamFlag Team { get; }
        
        int X { get; }
        int Y { get; }
        
        int MaxHealth { get; }
        int Health { get; }
        int MaxMana { get; }
        int Mana { get; }
        int ShieldStrength { get; } //Мой код.
        int MaxShieldStrength { get; } // Мой код.

        bool IsAlive();
        
        void AddMana(int mana);
        void SubMana(int mana);
        void Heal(int heal);  
        void Damage(int damage);
        void MoveTo(int x, int y);
        void Stun(int durationInTurns); //Мой код.
        void AddShieldStrength(int strength); //Мой код.
        void SubShieldStrength(int strength); //Мой код.

    }
}