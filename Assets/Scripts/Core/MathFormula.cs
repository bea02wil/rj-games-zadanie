﻿namespace Core
{
    public static class MathFormula
    {
        public static float GetValueFromPercentage(int value, float percent)
        {
            float result = (value * percent) / 100;          
           
            return result;
        }
    }
}
