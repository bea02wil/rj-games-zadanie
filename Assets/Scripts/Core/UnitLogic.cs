﻿using System;

namespace Core
{
    public abstract class UnitLogic
    {
        protected readonly IUnit Unit;
        protected readonly ICore Core;

        protected int unitTickCount; //Мой код.
        protected int takenDamageInTick; // Мой код.
      
        public int UnitTickCount { get => unitTickCount; set { unitTickCount = value; } } // Мой код. 
        public int TakenDamageInTurn { get => takenDamageInTick; set { takenDamageInTick = value; } } // Мой код.

        protected UnitLogic(IUnit unit, ICore core)
        {
            Unit = unit;
            Core = core;
        }

        public Action OnActionAfterDamageDelegate; //Мой код

        public virtual void OnSpawn()
        {
        }
    
        public virtual int OnDamage(int damage)
        {
            takenDamageInTick += damage;

            return damage;
        }

        public virtual int OnHeal(int heal)
        {
            if (!Unit.IsAlive()) //Мой код
            {
                return 0;
            }

            return heal;
        }

        public virtual int OnBeforeManaChange(int delta)
        {
            return delta;
        }
    
        public virtual void OnTurn()
        {
        }

        public virtual void OnAbility()
        {
            takenDamageInTick = 0;   // Мой код
        }

        public virtual void OnDie()
        {
        }

        public virtual void OnShield() // Мой код.
        {
        }

        public virtual void OnStun(int stunDurationInTick) //Мой код.
        {
            unitTickCount++;
         
            if (unitTickCount > stunDurationInTick || takenDamageInTick >= 100)
            {
                unitTickCount = 0;
               
                takenDamageInTick = 0;

                this.OnTurn();
            }

            if (takenDamageInTick != 0)
                takenDamageInTick = 0;
        }
        
        public virtual void OnMove() // Мой код
        {
            takenDamageInTick = 0;
        }
    }   
}