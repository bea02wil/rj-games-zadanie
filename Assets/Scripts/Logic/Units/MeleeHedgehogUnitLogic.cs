﻿using System;
using Conf;
using Core;

namespace Logic
{
    public class MeleeHedgehogUnitLogic : UnitLogic
    {
        private readonly int _attackDistance;
        private readonly int _healHpThreshold;
        private readonly int _healValue;
        private readonly int _abilityDamageIncreaseStep;
        private readonly int _damage;
        private readonly int _manaRegen;
        
        private int _abilityDamage;
    
        public MeleeHedgehogUnitLogic(MeleeHedgehogUnitInfo info, IUnit unit, ICore core) : base(unit, core)
        {
            _damage = info.Damage;
            _manaRegen = info.ManaRegen;
            _abilityDamage = 0;
            _healHpThreshold = (int)Math.Round(info.MaxHealth * info.HealHpThresholdPercent / 100f);
            _healValue = info.HealValue;
            _attackDistance = info.AttackDistance;
            _abilityDamageIncreaseStep = info.AbilityDamageIncreaseStep;
        }
    
        public override void OnTurn()
        {
            OnActionAfterDamageDelegate = null;  // Мой код

            takenDamageInTick = 0;  // Мой код

            var target = Core.GetNearestEnemy(Unit);
            if (target != null && target.IsAlive())
            {
                if (Core.GetDistance(Unit, target) > _attackDistance)
                {
                    Unit.MoveTo(target.X, target.Y);
                }
                else
                {
                    target.Damage(_damage);
                }
            }
            Unit.AddMana(_manaRegen);           
        }
      
        public override void OnAbility()
        {
            takenDamageInTick = 0;  // Мой код

            var target = Core.GetNearestEnemy(Unit);
            var canAttack = target != null && target.IsAlive(); // Мой измененный код.
            var isAttackInRange = Core.GetDistance(Unit, target) < _attackDistance; // Мой измененный код.

            if (canAttack && isAttackInRange)
            {               
                target.Damage(_abilityDamage);
                _abilityDamage = 0;

                //Блок кода вместо верхних двух строк на случай, если имелось ввиду, что урон должен идти суммарно с основным, т.е. с _damage,
                //предварительно убрав readonly с переменной _damage и суммировав с _abilityDamage в методе
                //под названием "OnBeforeManaChange(int delta)".
                //target.Damage(_damage);
                //_damage = 5;
                //_abilityDamage = 0;
            }           
        }
    
        public override int OnDamage(int damage)
        {
            //Если имелось ввиду, что всегда от максимального значения, т.е. 2500, то верно, иначе нужно было бы
            //пересчитывать перед проверкой урона переменную _healHpThreshold от текущего здоровья.
            if (damage > _healHpThreshold) //Мой измененный код. Было >= .
            {             
                OnActionAfterDamageDelegate = HealAfterDamage; // Мой код

                //Можно было и оставить как было, но в таком случае, если самая первая атака юнита вылетает критической
                //то хил не пройдет из-за того, что хил вызывается первым.
                //Unit.Heal(_healValue);
            }

            takenDamageInTick += damage;  // Мой код

            return damage;
        }
    
        public override int OnBeforeManaChange(int delta)
        {
            if (delta > 0) // А есть ли смысл проверять? - Мой вопрос.
            {
                _abilityDamage += _abilityDamageIncreaseStep;                
                //delta = 0; Здесь было это. Это не нужно.
            }
            return delta;
        }
        
        private void HealAfterDamage() // Мой код
        {
            Unit.Heal(_healValue);
        }
    }
}