﻿using Conf;
using Core;
using System;

namespace Logic
{
    public class MeleeMonkUnitLogic : UnitLogic //Мой класс. 
    {
        private readonly int _attackDistance;
        private readonly int _damage;
        private readonly int _manaRegen;
        private readonly int _shieldStrength;
        private readonly int _healValue;
      
        public MeleeMonkUnitLogic(MeleeMonkUnitInfo info, IUnit unit, ICore core) : base(unit, core)
        {
            _damage = info.Damage;
            _manaRegen = info.ManaRegen;
            _attackDistance = info.AttackDistance;
            _shieldStrength = info.ShieldStrength;
            _healValue = info.HealValue;       
        }

        public override void OnTurn()
        {
            OnActionAfterDamageDelegate = null;
            
            var target = Core.GetNearestEnemy(Unit);

            var canAttack = target != null && target.IsAlive();
            var isAttackInRange = Core.GetDistance(Unit, target) <= _attackDistance;

            if (canAttack)
            {
                if (isAttackInRange)
                {
                    target.Damage(_damage);
                }
                else
                {
                    Unit.MoveTo(target.X, target.Y);
                }
            }

            Unit.AddMana(_manaRegen);
            
            if (IsDamaged())
            {
                var healPercent = 10f;

                HealForPercentOfTakenDamage(healPercent, takenDamageInTick);

                takenDamageInTick = 0;
            }
        }

        public override void OnSpawn()
        {
            Unit.AddShieldStrength(_shieldStrength);           
        }

        public override int OnDamage(int damage)
        {
            if (!IsShieldBroken())
            {
                var percentDamageToReduce = 50f;
                var shieldReducedDamageValue = (int)MathFormula.GetValueFromPercentage(damage, percentDamageToReduce);

                Unit.SubShieldStrength(shieldReducedDamageValue);

                if (IsShieldBroken())                
                    damage = damage - shieldReducedDamageValue + Math.Abs(Unit.ShieldStrength);
                
                else
                    damage -= shieldReducedDamageValue;
            }

            takenDamageInTick += damage;

            //Можно было сделать и в методе OnDie(), но тогда бы хил прошел на след. ходе, а так на текущем.
            OnActionAfterDamageDelegate += HealNearestFriendOnCurrentShieldStrengthValue;
           
            return damage;
        }

        public override int OnBeforeManaChange(int delta)
        {
            if (!IsShieldBroken())
            {
                Unit.AddShieldStrength(delta);
            }

            return delta;
        }

        public override void OnShield()
        {
            if (IsShieldBroken())
            {
                var damageToNearestEnemy = 250;
                var healthToRestore = _healValue;

                ExploseShield(damageToNearestEnemy, healthToRestore);
            }          
        }

        public override void OnStun(int stunDurationInTurns)
        {
            if (IsDamaged())
            {
                var healPercent = 10f;

                HealForPercentOfTakenDamage(healPercent, takenDamageInTick);             
            }

            base.OnStun(stunDurationInTurns);
        }

        private bool IsShieldBroken()
        {      
            return Unit.ShieldStrength <= 0;
        }
      
        private void ExploseShield(int damageToNearestEnemy, int healthToRestore)
        {
            var target = Core.GetNearestEnemy(Unit);
            var canDamage = target != null && target.IsAlive();

            if (canDamage)
            {
                target.Damage(damageToNearestEnemy);
            }
            
            OnActionAfterDamageDelegate += () => Unit.Heal(healthToRestore);
        }

        private bool IsDamaged()
        {
            return takenDamageInTick > 0;
        }

        private void HealNearestFriendOnCurrentShieldStrengthValue()
        {
            if (!Unit.IsAlive() && !IsShieldBroken())
            {
                var friend = Core.GetNearestFriend(Unit);
                var canHeal = friend != null && friend.IsAlive();

                if (canHeal)
                {
                    var currentShieldStrength = Unit.ShieldStrength;

                    friend.Heal(currentShieldStrength);
                }
            }          
        }

        private void HealForPercentOfTakenDamage(float healPercent, int takenDamage)
        {
            var healValue = MathFormula.GetValueFromPercentage(takenDamage, healPercent);
            var healValueCeilingRound = (int)Math.Ceiling((double)healValue);
         
            Unit.Heal(healValueCeilingRound);
        }
    }
}

