﻿using Conf;
using Core;

namespace Logic
{
    public class RangeArcherUnitLogic : UnitLogic
    {
        private readonly int _attackDistance;
        private readonly int _killChance;
        private readonly int _damage;
        private readonly int _manaRegen;
        

        public RangeArcherUnitLogic(RangeArcherUnitInfo info, IUnit unit, ICore core) : base(unit, core)
        {
            _damage = info.Damage;
            _manaRegen = info.ManaRegen;
            _attackDistance = info.AttackDistance;
            _killChance = info.KillChance;
        }
    
        public override void OnTurn()
        {
            takenDamageInTick = 0; // Мой код.

            var target = Core.GetNearestEnemy(Unit);
            if (target != null && target.IsAlive())
            {
                if (Core.GetDistance(Unit, target) > _attackDistance)
                {
                    Unit.MoveTo(target.X, target.Y);
                }
                else
                {
                    target.Damage(_damage);
                }
            }
            Unit.AddMana(_manaRegen);      
        }
    
        public override void OnAbility()
        {
            takenDamageInTick = 0; // Мой код.

            var target = Core.GetNearestEnemy(Unit);
            var canImmediatelyKill = UnityEngine.Random.Range(0, 100) < _killChance; // Мой изменненый код.
            var canAttack = target != null && target.IsAlive(); // Мой изменненый код.

            if (canAttack)
            {
                if (canImmediatelyKill)
                {
                    target.Damage(target.MaxHealth);

                    if (target.IsAlive()) // Мой код.
                    {
                        target.Stun(3);
                    }
                }
                else
                {
                    //Пусть, если шанс не был равен 25%, цель получит обыкновенный урон, а то просто пропустит ход на "ультимативной" способности.
                    target.Damage(_damage);                   
                }
            }
           
            //На случай, если лучник атакует способностью также по дистанции.

            //if (canAttack)
            //{
            //    if (Core.GetDistance(Unit, target) > _attackDistance)               
            //        Unit.MoveTo(target.X, target.Y);

            //    else
            //    {
                    //if (canImmediatelyKill)
                    //{
                    //    target.Damage(target.MaxHealth);

                    //    if (target.IsAlive())
                    //    {
                    //        target.Stun(3);
                    //    }
                    //}

            //        else
            //        {
            //            target.Damage(_damage);                                         
            //        }
            //    }
            //}
        }

        public override int OnDamage(int damage)
        {
            Unit.AddMana(_manaRegen);

            takenDamageInTick += damage; // Мой код.

            return damage;
        }        
    }
}